import './App.css';
import Login from './Components/Login';
import { BrowserRouter as Router, Route, Link , Switch } from "react-router-dom";
import FoodMenu from './Components/FoodMenu';

function App() {
  
  return (
    // <Router>
    //   <nav>
    //     <Link to="/">Home</Link>
    //     <Link to="/about">About</Link>
    //     <Link to="/users">Users</Link>
    //   </nav>
    //   <Switch>
    //     <Route path="/about">
    //       <Login/>
    //     </Route>
    //     <Route path="/users">
    //       <FoodMenu />
    //     </Route>
    //     <Route path="/">
    //       <Login />
    //     </Route>
    //   </Switch>
    // </Router>
    <div className="App">
      <Login/>
    </div> 
  )
}

export default App;
