import React, { useEffect, useState } from 'react';
import FoodMenu from './FoodMenu';
import './styles/Login.css';
import { Button } from 'react-bootstrap';
import axios from 'axios';

var items = [];

const fetchData = (userid, username, password) => {
  axios.post('http://localhost:8080/surabi/customer/login', {
    "id": userid,
    "name": username,
    "password": password
  }).then((response) => {
    items = response.data;
  });
  localStorage.setItem("items", items)
}

function Login() {

  const [userid, setUserId] = useState('')
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [validSign, setValidSign] = useState(false)
  const [validRegister, setValidRegister] = useState(false)

  useEffect(() => {
    fetchData(userid, username, password)
  })

  const handleChange1 = (e) => {
    setUsername(e.target.value)
  }

  const handleChange2 = (e) => {
    setPassword(e.target.value)
  }

  const handleChange3 = (e) => {
    setUserId(e.target.value)
  }

  const handleSignIn = (e) => {
    if(username.length == 0 || userid.length == 0 || password.length == 0){
      alert("Please provide all the details first !");
      return null ;
    }else if(items.length == 0){
      alert("something wentwrong, please try again !!!")
      return null;
    }
    e.preventDefault()
    console.log(username + " SignIn " + password)
    setValidSign(true);
  }

  const handleRegister = (e) => {
    console.log(username + " Register " + password)
    axios.post('http://localhost:8080/surabi/customer/register', {
      "id": userid,
      "name": username,
      "password": password
    }).then((response) => {
      console.log(response);
    });
    document.getElementById('userId').value = ''
    document.getElementById('email').value = ''
    document.getElementById('pwd').value = ''
    setValidRegister(true);
  }

  const handleLogOut = (e) => {
    console.log("User Successfully Logged Out");
    var bill = localStorage.getItem("bill")
    axios.get('http://localhost:8080/surabi/customer/order/'+bill, 
    ).then((response) => {
      alert(JSON.stringify(response.data));
      console.log(response);
    });
    document.getElementById('userId').value = ''
    document.getElementById('email').value = ''
    document.getElementById('pwd').value = ''
    setValidSign(false);
    localStorage.removeItem("items");
  }

  return (
    <div className='div-login'>
      <div className="heading">
        <h1>Surabi</h1>
        <h1 id="rest">Restaurant</h1></div>
      <div className='Title'>
        <div>
          <form>
            <div className="input1">
              <input type='text' name='userId' id="userId" placeholder='userId...' required onChange={handleChange3} />
            </div>
            <div className="input2">
              <input type='email' name='email' id="email" placeholder='email...' required onChange={handleChange1} />
            </div>
            <div className="input3">
              <input type='password' name='pwd' id="pwd" placeholder='password...' required onChange={handleChange2} />
            </div>
            <div className="buton">
              <Button className="signin" variant="success" onClick={handleSignIn}>Sign In</Button>{' '}
              <Button className="register" variant="warning" onClick={handleRegister}>Register</Button>{' '}
            </div>
          </form>
        </div>
        <div className="foodMenu">
          {validSign ? <FoodMenu initialItems={items} />
            : null}
          {validSign ? <Button variant="danger" onClick={handleLogOut}>LogOut</Button>
            : null}
        </div>
      </div>
    </div>
  )
}

export default Login;