import React from 'react'
import './styles/FoodItem.css';

function FoodItem(props) {
  let qty = props.qty || 0;
  const addOne = () => props.updateQty(props.id, qty + 1);
  const subtractOne = () => props.updateQty(props.id, qty - 1);
  return (
    <div className="CartItem">
      <div>{props.itemName}</div>
      <div>₹{props.price}</div>
      <div>
        <button onClick={subtractOne} disabled={qty <= 0}>
          -
        </button>
        {qty}
        <button onClick={addOne}>+</button>
      </div>
      <div>Total: ₹{qty * props.price}</div>
    </div>
  )
}
export default FoodItem