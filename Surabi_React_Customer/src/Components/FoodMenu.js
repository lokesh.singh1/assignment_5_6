import React, { useEffect, useState } from 'react';
import Table from 'react-bootstrap/Table'
import FoodItem from './FoodItem';
import './styles/FoodMenu.css';

function FoodMenu({ initialItems }) {
    const initialState = localStorage.getItem("items");
    const [items, setItems] = useState(initialItems || initialState);
    const [grandTotal,setGrandTotal] = useState(0);

    useEffect(() => {
        var temp = 0;
        items.map((item) => {
            if (item.qty) {
                temp += item.qty * item.price;
            }
        });
        setGrandTotal(temp);
        localStorage.setItem("bill",grandTotal);
    });

    const updateQty = (id, newQty) => {
        const newItems = items.map(item => {
            if (item.id === id) {
                return { ...item, qty: newQty };
            }
            return item;
        });
        setItems(newItems);
    };

    return (
        <div className="Cart">
            <h1 className="Cart-title">Food Menu</h1>
            <div className="Cart-items">
                {items.map(item => (
                    <FoodItem key={item.id} updateQty={updateQty} {...item} />
                ))}
            </div>
            <h2 className="Bill">Total: ₹{grandTotal}</h2>
        </div>
    )
}

export default FoodMenu;