-> Please check the SQL Connection driver before running the application.

-> Run SURABI_SQL_SCRIPT.sql script given in this folder to create the database. 
  {Four tables will get created using this sql script named userbills, users, admin and fooditems in "surabi" database} 

#please change the credentials in the application-properties, to establish database connection.

-> Run SpringBoot project present in the given repository namely SurabiAssignment3 using eclipse/IDE.

# For invigilator convinience Iam providing POSTMAN collection to run and check Api endPoints directly.

-> Link: https://www.getpostman.com/collections/1295b3f09a05888be71f

	->in the collection respective API's are provided with respective methods and request bodies if required.


-> if have created separate folders for ENTITES,DAO,SERVICES,CONTROLLER,APPLICATION.

-> make sure your running API's in the correct order i mean login then followed by other API's

-> logout after use.

#I extends JPA_Repository interface to provide CURD operations to the admin and Hibernate orm for creating sessions 
 and performing all other functionalities.

#kEY_POINTS
1.In this application, I've categorized users into customer and admin.
2. All the user creation in done using SQL querries as given in the .sql script.
3. One of existing user that will be created after running .sql script will be id: "101", name: "loki", pass: "123"

