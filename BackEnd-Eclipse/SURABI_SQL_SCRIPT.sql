CREATE SCHEMA surabi ;

CREATE TABLE `surabi`.`foodmenu` (
  `ItemNumber` INT NOT NULL,
  `ItemName` VARCHAR(45) NULL,
  `Price` INT NULL, PRIMARY KEY(ItemNumber));

CREATE TABLE `surabi`.`users` (
  `id` INT NOT NULL,
  `userName` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`,`userName`));

CREATE TABLE `surabi`.`userbills` (
  `SerialNo` INT NOT NULL AUTO_INCREMENT,
  `Date` DATE NOT NULL,
  `UserName` VARCHAR(45) NOT NULL,
  `Bill` INT NOT NULL,
  PRIMARY KEY (`SerialNo`));

CREATE TABLE `surabi`.`admin` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

INSERT INTO `surabi`.`foodmenu` (`ItemNumber`, `ItemName`, `Price`) VALUES ('101', 'pizza', '200');
INSERT INTO `surabi`.`foodmenu` (`ItemNumber`, `ItemName`, `Price`) VALUES ('102', 'burger', '100');
INSERT INTO `surabi`.`foodmenu` (`ItemNumber`, `ItemName`, `Price`) VALUES ('103', 'pasta', '150');
INSERT INTO `surabi`.`foodmenu` (`ItemNumber`, `ItemName`, `Price`) VALUES ('104', 'noodles', '120');

INSERT INTO `surabi`.`users` (`id`, `userName`, `password`) VALUES ('1', 'loki', '123');
INSERT INTO `surabi`.`users` (`id`, `userName`, `password`) VALUES ('2', 'parth', '123');
INSERT INTO `surabi`.`users` (`id`, `userName`, `password`) VALUES ('3', 'rohit', '123');
INSERT INTO `surabi`.`users` (`id`, `userName`, `password`) VALUES ('4', 'vinayak', '321');

INSERT INTO `surabi`.`admin` (`id`, `username`, `password`) VALUES ('101', 'rahul', '123');
INSERT INTO `surabi`.`admin` (`id`, `username`, `password`) VALUES ('102', 'prachi', '321');
INSERT INTO `surabi`.`admin` (`id`, `username`, `password`) VALUES ('103', 'loki', '121');
