package com.GreatLearning.SurabiAssignment3LS.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.boot.context.properties.ConstructorBinding;

import lombok.Getter;
import lombok.Setter;

//Customer entity to provide and map all the data and fields of our database table 'users' 
//containing Customer's credentials.
@Entity
@Table(name = "users")
public class Customer{
	
	@Id
	@Column(name = "id")
  	private int id;
	@Column(name = "username")
	private String name;
	@Column(name = "password")
	private String password;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
