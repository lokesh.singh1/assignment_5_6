package com.GreatLearning.SurabiAssignment3LS.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//foodMenu entity to provide and map all the data and fields of our database table 'foodmenu' 
//containing all the available foodNames with price a customer can order.
@Entity
@Table(name = "foodmenu")
public class FoodMenu {
	
	@Id
	@Column(name ="id")
	private int id;
	@Column(name = "item")
	private String itemName;
	@Column(name = "price")
	private int price;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
}
