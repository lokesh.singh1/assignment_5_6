package com.GreatLearning.SurabiAssignment3LS.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.GreatLearning.SurabiAssignment3LS.entity.Admin;
import com.GreatLearning.SurabiAssignment3LS.entity.Customer;

//Using JpaRepository to use the predefined sql querry methods for providing CURD operations to Admin

public interface AdminCURD extends JpaRepository<Customer, String> {
	
}
