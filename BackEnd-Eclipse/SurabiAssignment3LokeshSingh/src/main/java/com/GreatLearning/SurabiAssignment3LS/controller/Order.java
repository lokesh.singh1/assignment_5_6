package com.GreatLearning.SurabiAssignment3LS.controller;

import java.util.List;

//To take order from user in the form of this 'order' class object
public class Order {
	
	//Customer will provide itemNumber of foodItem in a LinkedList of Integer through this class object.
	private List<Integer> order ;

	public List<Integer> getOrder() {
		return order;
	}
 
	public void setOrder(List<Integer> order) {
		this.order = order;
	}
}
