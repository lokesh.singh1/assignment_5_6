package com.GreatLearning.SurabiAssignment3LS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SurabiAssignment3LSApplication {

	public static void main(String[] args) {
		SpringApplication.run(SurabiAssignment3LSApplication.class, args);
	}

}
