import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Admin } from '../model/admin';
const baseURL = 'http://localhost:8080/surabi/admin/user';
@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private httpClient : HttpClient) { }
  getAllAdmins(){
    return this.httpClient.get<Admin[]>('http://localhost:8080/surabi/admin');
  }
  // service methods for crud on users

  readAll() {
    return this.httpClient.get('http://localhost:8080/surabi/admin/users');
  }

  // read(id){
  //   return this.httpClient.get(`${baseURL}/${id}`);
  // }

  create(data : any) {
    console.log(data)
    return this.httpClient.post(`${baseURL}`, data,{responseType : "text"});
  }

  update(options : any){
    return this.httpClient.put(`${baseURL}`, options,{responseType : "text"});
  }

  delete(options : any){
    return this.httpClient.delete(`${baseURL}`,options)
  }
}
