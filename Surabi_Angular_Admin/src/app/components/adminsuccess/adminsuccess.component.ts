import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Bill } from 'src/app/model/bill';
import { BillserviceService } from 'src/app/services/billservice.service';

@Component({
  selector: 'app-adminsuccess',
  templateUrl: './adminsuccess.component.html',
  styleUrls: ['./adminsuccess.component.css']
})
export class AdminsuccessComponent implements OnInit {

  constructor(private billService : BillserviceService,private router : Router) { }
  name : string = "";
  ngOnInit(): void {
    this.name = history.state.data;
  }
  titles: string [] = ["Sr.No","Month", "Date of order", "Price"];
  index: string[] = ["id","month","date","price"];
  bills : Bill[]  = [];
  counter : number = 0;
  currentMonthSale : string = ""; 
  d = new Date();
  curmonth : number = this.d.getMonth() + 1;
  curdate : string = this.d.toISOString().slice(0, 10);
  showBills : boolean = false;
  showSale : boolean = false;
  rowcount : number = 1;

  getCurrMonthSale() {
    this.billService.getCurrMonthSale()
    .subscribe((data : any) => {
      this.showSale = true;
      this.showBills = false;
      this.currentMonthSale = data;
    },(err) => {
      console.log(err);
    })
  }
  getTodayBills() {
      this.billService.getAllBills()
      .subscribe((data : Bill[]) => {
        this.bills = data.filter((bill)=> {
            return this.curdate === bill.date;
        })
        this.rowcount = 1;
        this.bills.forEach((bill) => {
          bill.id = this.rowcount;
          this.rowcount++;
        })
        this.showSale = false;
        if (this.bills.length > 0) {
          this.showBills = true;
        }
        else {
          alert('There are no bill generated today!!')
        }
      },(error) => {
        console.log(error);
      })
  }



  redirectToHome() {
    this.router.navigate(["/"]);
  }

}
