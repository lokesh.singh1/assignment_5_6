import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  constructor(private adminService : AdminService,private router : Router) { }
  users : any;
  header  = ["Id","Name", "Password","Edit","Delete"];
  ngOnInit(): void {
    this.getAllUsers();
  }

  options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    body: {},
    responseType : 'text'
  };
  getAllUsers(): void {
    this.adminService.readAll()
      .subscribe(
        users => {
          this.users = users;
          console.log(users);
        },
        error => {
          console.log(error);
        });
  }
  editUser (user: any): void {
    this.router.navigate(["/create"],{state: {data: {...user}}});
  }
  deleteUser(user : any): void {
    this.options.body = user;

    console.log(this.options)
    this.adminService.delete(this.options)
      .subscribe(
        response => {
          console.log(response);
          this.ngOnInit();
        },
        error => {
          console.log(error);
        });
  }

  redirectToSuccess() {
    this.router.navigate(['success']);
  }
}
