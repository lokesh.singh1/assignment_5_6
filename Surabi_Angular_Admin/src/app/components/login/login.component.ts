import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Admin } from 'src/app/model/admin';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  admin: Admin = new Admin();
  admins : Admin[] = [];
  constructor(private adminService : AdminService,private router : Router) { }

  ngOnInit(): void {
  }
  authenticateUser() {
    this.adminService.getAllAdmins()
    .subscribe((data : Admin[]) => {
      console.log(data);
      this.admins = data;
      let currentUser = this.admins.filter(admin => {
        return admin.name === this.admin.name && admin.password === this.admin.password;
      })

      if (currentUser.length > 0) {
        this.router.navigate(["success"],{state: {data: this.admin.name}});
      }
      else {
        alert("Invalid credentials! Please try again");
      }
    })
  }
}
