import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-usercreate',
  templateUrl: './usercreate.component.html',
  styleUrls: ['./usercreate.component.css']
})
export class UsercreateComponent implements OnInit {

  user = {
    id : '',
    name: '',
    password: '',
  };
  submitted = false;
  constructor(private adminService : AdminService,private router : Router) { }

  ngOnInit(): void {
    if (history.state.data)
    this.user = history.state.data;
  }

  // options = {
  //   headers: new HttpHeaders({
  //     'Content-Type': 'application/json',
  //   }),
  //   body: {},
  //   responseType : 'text'
  // };
  newData = {}
  createUser(): void {
    let data = {
      name: this.user.name,
      password: this.user.password
    };
    if (history.state.data) {
      this.newData = {id : history.state.data.id,...data}
      console.log(history.state.data.id);
      // this.options.body = this.newData;
      // console.log(this.options);
      this.adminService.update(this.newData)
      .subscribe(
        response => {
          console.log(response);
          alert('Product was updated!')
          this.router.navigate(['/users']);
        },
        error => {
          console.log(error);
        });
    }
    else {
        //this.options.body = data;
        //console.log(this.options);
        this.adminService.create(data)
          .subscribe(
            response => {
              console.log(response);
              this.submitted = true;
            },
            error => {
              console.log(error);
            });
      }
  }

  newUser(): void {
    this.submitted = false;
    this.user = {
      id : '',
      name: '',
      password: ''
    };
  }

  redirectToSuccess() {
    this.router.navigate(['success']);
  }

}
